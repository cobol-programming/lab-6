       IDENTIFICATION DIVISION. 
       PROGRAM-ID.  WRITE-EMP1.
       AUTHOR. KANTIDA.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT EMP-FILE ASSIGN TO "emp1.dat"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION.
       FD  EMP-FILE.
       01  EMP-DETAILS.
           88 END-OF-EMP_FILE VALUE HIGH-VALUE.
           05 EMP-SSN PIC 9(9).
           05 EMP-NAME.
              10 EMP-SURNAME PIC X(15).
              10 EMP-FORNAME PIC X(10).
           05 EMP-DATE-OF-BIRTH.
              10 EMP-YOB PIC 9(4).
              10 EMP-MOB PIC 9(2).
              10 EMP-DOB PIC 9(2).
           05 EMP-GENDER PIC X.

       PROCEDURE DIVISION.
       BEGIN.
           OPEN OUTPUT EMP-FILE 
           MOVE "123456789" TO EMP-SSN 
           MOVE "KANTIDA" TO EMP-FORNAME 
           MOVE "PATKACHA" TO EMP-SURNAME 
           MOVE "2001" TO EMP-YOB 
           MOVE "04" TO EMP-MOB 
           MOVE "13" TO EMP-DOB 
           MOVE "F" TO EMP-GENDER
           WRITE EMP-DETAILS

           MOVE "987654321" TO EMP-SSN 
           MOVE "ATIKAN" TO EMP-FORNAME 
           MOVE "PATKACHA" TO EMP-SURNAME  
           MOVE "2003" TO EMP-YOB 
           MOVE "01" TO EMP-MOB 
           MOVE "10" TO EMP-DOB 
           MOVE "M" TO EMP-GENDER
           WRITE EMP-DETAILS

           MOVE "456789123" TO EMP-SSN 
           MOVE "Nut" TO EMP-FORNAME  
           MOVE "Nuttakit" TO EMP-SURNAME  
           MOVE "1993" TO EMP-YOB 
           MOVE "03" TO EMP-MOB 
           MOVE "26" TO EMP-DOB 
           MOVE "M" TO EMP-GENDER
           WRITE EMP-DETAILS
           CLOSE EMP-FILE
           GOBACK
           .